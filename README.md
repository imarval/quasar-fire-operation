# quasar-fire-operation
Triangula la posicion de un punto en el espacio y calcula mensaje con interferencia

Ir a raiz del proyecto
Abrir consola y ejecutar go run *.go
Por defecto el servicio esta corriendo en localhost:8080

## Demo https://quasar-fire-operation.herokuapp.com/

# Endpoints

```go
/topsecret  POST
{
    "satellites": [
        {
            "name": "kenobi",
            "distance": 10.0,
            "message": ["esto", "", "un", "mensaje"]
        },
        {
            "name": "skywalker",
            "distance": 10.0,
            "message": ["", "", "un", ""]
        },
        {
            "name": "sato",
            "distance": 22.523245,
            "message": ["", "es", "", "mensaje"]
        }
    ]
}

/topsecret_split/{satellite_name} POST
{
    "distance": 10.0,
    "message": ["esto", "", "", "mensaje"]
}

/topsecret_split  GET
```
