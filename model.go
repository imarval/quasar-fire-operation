package main

type Satellite struct {
	Name     string   `json:"name"`
	Distance float32  `json:"distance"`
	Message  []string `json:"message"`
}

type Satellites struct {
	Satellites []Satellite `json:"satellites"`
}

type Position struct {
	X float32 `json:"x"`
	Y float32 `json:"y"`
}

type Circle struct {
	Position Position
	Radius   float32
}

type Response struct {
	Position Position `json:"position"`
	Message  string   `json:"message"`
}
