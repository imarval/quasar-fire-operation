package main

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"math"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
)

func GetMessage(messages ...[]string) (msg string) {
	var message string
	var auxMsj []string
	lenMessage := len(messages[0])

	for i := 0; i < lenMessage; i++ {
		for _, msj := range messages {
			if msj[i] != "" {
				auxMsj = append(auxMsj, msj[i])
			}
		}
		if len(auxMsj) > 0 && AllSameStrings(auxMsj) {
			message += auxMsj[0] + " "
			auxMsj = nil
			continue
		}
		message += ""
	}

	if message == "" || (strings.Trim(message, " ")) == "" {
		return ""
	}
	return strings.TrimRight(message, " ")
}

func GetLocation(distances ...float32) (x, y float32, err error) {
	var kenobi = Circle{SatellitesPosition[0], float32(math.Sqrt(float64(distances[0])))}
	var skywalker = Circle{SatellitesPosition[1], float32(math.Sqrt(float64(distances[1])))}
	var sato = Circle{SatellitesPosition[2], float32(math.Sqrt(float64(distances[2])))}

	location, err := FindIntersectionsPoints(kenobi, skywalker, sato)

	if err != nil {
		return -1, math.MaxFloat32, err
	}

	return location.X, location.Y, nil
}

func AllSameStrings(value []string) bool {
	for i := 0; i < len(value); i++ {
		if value[i] != value[0] {
			return false
		}
	}
	return true
}

func FindSourceMessageLocation(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "Bad Request", http.StatusBadRequest)
	}

	var satellites Satellites
	err = json.Unmarshal(body, &satellites)
	if err != nil {
		http.Error(w, "Bad Request", http.StatusBadRequest)
	}
	if len(satellites.Satellites) < 3 {
		http.Error(w, "No hay suficiente informacion", http.StatusNotFound)
	}

	x, y, message, err := FindShipLocation(satellites.Satellites)

	if err != nil {
		http.Error(w, "No se puede determinar la posicion de la nave", http.StatusNotFound)
	} else if message == "" {
		http.Error(w, "No se puede determinar el mensaje", http.StatusNotFound)
	} else {
		res := Response{
			Position: Position{x, y},
			Message:  message,
		}

		w.Header().Set("content-type", "application/json")
		json.NewEncoder(w).Encode(res)
	}
}

func FindShipLocation(satellites []Satellite) (x, y float32, message string, err error) {
	var rKenobi, rSkywalker, rSato float32
	messages := [][]string{}

	for _, s := range satellites {
		name := strings.ToUpper(s.Name)
		switch name {
		case "KENOBI":
			rKenobi = s.Distance
			messages = append(messages, s.Message)
		case "SKYWALKER":
			rSkywalker = s.Distance
			messages = append(messages, s.Message)
		case "SATO":
			rSato = s.Distance
			messages = append(messages, s.Message)
		}
	}

	x, y, err = GetLocation(rKenobi, rSkywalker, rSato)

	message = GetMessage(messages...)

	return x, y, message, err

}

func FindIntersectionsPoints(kenobi, skywalker, sato Circle) (Position, error) {
	dx := skywalker.Position.X - kenobi.Position.X
	dy := skywalker.Position.Y - kenobi.Position.Y

	var d = GetDistance(Position{kenobi.Position.X, kenobi.Position.Y}, Position{skywalker.Position.X, skywalker.Position.Y})

	// Se valida el tipo de interseccion en base a la distancia y el valor de sus radios
	// FAIL 1.- La distancia entre los centros es mayor que la suma de sus radios -> No se interceptan
	// FAIL 2.- La distancia entre los centros es mayor que cero y menor que la diferencia entre sus radios -> Un circulo esta dentro del otro
	// FAIL 3.- La distancia es igual que cero y los radios la misma longitud -> Son la misma circunferencia
	// OK 4.- La distancia es igual que la suma de sus radios
	// OK 5.- La distancia es menor que la suma de sus radios

	if d > kenobi.Radius+skywalker.Radius || d < float32(math.Abs(float64(kenobi.Radius-skywalker.Radius))) || d == 0 && kenobi.Radius == skywalker.Radius {
		return Position{}, errors.New("No se puede hallar la ubicacion de la nave")
	}

	// Se calcula la distancia al punto donde la linea que atraviesa los puntos
	// de interseccion del circulo y cruza la linea entre los centros de circulo
	a := ((kenobi.Radius * kenobi.Radius) - (skywalker.Radius * skywalker.Radius) + (d * d)) / (2 * d)

	// Se calcula las coordenadas del punto
	x0 := kenobi.Position.X + (dx * a / d)
	y0 := kenobi.Position.Y + (dy * a / d)

	// Se calcula la distancia del punto a otro de los puntos de interseccion
	h := float32(math.Sqrt(float64(kenobi.Radius*kenobi.Radius) - float64(a*a)))

	// Se calcula las compensaciones de los puntos de interseccion desde ese punto
	rx := -dy * (h / d)
	ry := dx * (h / d)

	// Se determina los puntos de interseccion
	intersection1 := Position{x0 + rx, y0 + ry}
	intersection2 := Position{x0 - rx, y0 - ry}

	// Se valida que el circulo 3 atraviesa en cualquiera de los dos puntos de interseccion anteriores
	dx = intersection1.X - sato.Position.X
	dy = intersection1.Y - sato.Position.Y

	d1 := float32(math.Sqrt(float64((dy * dy) + (dx * dx))))

	dx = intersection2.X - sato.Position.X
	dy = intersection2.Y - sato.Position.Y

	d2 := GetDistance(Position{sato.Position.X, sato.Position.Y}, intersection2)

	// Si el valor absoluto entre la distancia del punto de interseccion
	// al centro del circulo 3 con su radio es 0 lo intercepta
	// Se valida con 0.01 como margen de error de decimales
	if math.Abs(float64(d1-sato.Radius)) < 0.01 {
		return intersection1, nil
	} else if math.Abs(float64(d2-sato.Radius)) < 0.01 {
		return intersection2, nil
	}
	return Position{}, errors.New("No se puede hallar la ubicacion de la nave")
}

func GetDistance(a, b Position) float32 {
	var dx = float64(b.X - a.X)
	var dy = float64(b.Y - a.Y)
	return float32(math.Sqrt(dx*dx + dy*dy))
}

func NewSatelliteInfo(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, "Bad Request", http.StatusBadRequest)
	}
	vars := mux.Vars(r)
	satelliteName := vars["satellite_name"]

	satellite := Satellite{satelliteName, 0, []string{}}
	err = json.Unmarshal(body, &satellite)
	if err != nil {
		http.Error(w, "Bad Request", http.StatusBadRequest)
	}

	if len(allSatellites) <= 3 && len(allSatellites) > 0 {
		if SatelliteExist(satelliteName) {
			UpdateSatelliteSplit(satellite)
		} else {
			allSatellites = append(allSatellites, satellite)
		}
	}

	w.Header().Set("content-type", "application/json")
	json.NewEncoder(w).Encode(allSatellites)
}

func SatelliteExist(name string) bool {
	for _, s := range allSatellites {
		if strings.ToUpper(name) == strings.ToUpper(s.Name) {
			return true
		}
	}
	return false
}

func UpdateSatelliteSplit(satellite Satellite) {
	for i, s := range allSatellites {
		if strings.ToUpper(satellite.Name) == strings.ToUpper(s.Name) {
			allSatellites = append(allSatellites[:i], allSatellites[i+1:]...)
			allSatellites = append(allSatellites, satellite)
		}
	}
}

func GetLocationBySplit(w http.ResponseWriter, r *http.Request) {
	x, y, message, err := FindShipLocation(allSatellites)

	if err != nil {
		http.Error(w, "No se puede determinar la posicion de la nave", http.StatusNotFound)
	} else if message == "" {
		http.Error(w, "No se puede determinar el mensaje", http.StatusNotFound)
	} else {
		res := Response{
			Position: Position{x, y},
			Message:  message,
		}

		w.Header().Set("content-type", "application/json")
		json.NewEncoder(w).Encode(res)
	}
}
