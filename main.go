package main

import (
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

func main() {
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/topsecret", FindSourceMessageLocation).Methods("POST")
	router.HandleFunc("/topsecret_split/{satellite_name}", NewSatelliteInfo).Methods("POST")
	router.HandleFunc("/topsecret_split", GetLocationBySplit).Methods("GET")

	port := os.Getenv("PORT")

	if port == "" {
		port = "8080"
	}

	log.Fatal(http.ListenAndServe(":"+port, router))
}
